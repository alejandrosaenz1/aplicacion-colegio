package com.crud.h2.excepciones;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResorceNotFoundException extends RuntimeException {

	//clase personalizada de excepciones
	private static final long serialVersionID = 1L;

	public ResorceNotFoundException(String mensaje) {

		super(mensaje);
	}

}
