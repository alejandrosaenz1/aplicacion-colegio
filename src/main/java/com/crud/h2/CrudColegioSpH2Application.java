package com.crud.h2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudColegioSpH2Application{


	public static void main(String[] args) {
		SpringApplication.run(CrudColegioSpH2Application.class, args);
	}

}
