package com.crud.h2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import antlr.collections.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Profesor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String nombre;
	@Column(nullable = false)
	private String asignaturas;
	
	public Profesor() {
		
	}

	public Profesor(Long id, String nombre, String asignaturas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.asignaturas = asignaturas;
	}
	public Profesor(String nombre, String asignaturas) {
		super();
		this.nombre = nombre;
		this.asignaturas = asignaturas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAsignaturas() {
		return asignaturas;
	}

	public void setAsignaturas(String asignaturas) {
		this.asignaturas = asignaturas;
	}
	
	@Override
	public String toString() {
		return "Profesor [id=" +id+", nombre=" +nombre+ ", asignaturas=" +asignaturas+ "]";
	}



	public void addAttribute(String string, java.util.List<Profesor> listaTodosLosProfesores) {
		// TODO Auto-generated method stub
		
	}
	
}
