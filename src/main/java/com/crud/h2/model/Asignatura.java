package com.crud.h2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import antlr.collections.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Asignatura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String nombre;
	@Column(nullable = false)
	private String profesor;
	@Column(nullable = false)
	private String estudiantes;
	@Column(nullable = false)
	private String curso;
	
	public Asignatura() {
		
	}

	public Asignatura(Long id, String nombre, String profesor, String estudiantes, String curso) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.profesor = profesor;
		this.estudiantes = estudiantes;
		this.curso = curso;
	}
	public Asignatura(String nombre, String profesor, String estudiantes, String curso) {
		super();
		this.nombre = nombre;
		this.profesor = profesor;
		this.estudiantes = estudiantes;
		this.curso = curso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	public String getEstudiantes() {
		return estudiantes;
	}

	public void setEstudiantes(String estudiantes) {
		this.estudiantes = estudiantes;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	@Override
	public String toString() {
		return "Asignatura [id=" +id+", nombre=" +nombre+ ", profesor=" +profesor+", estudiantes=" +estudiantes+", curso=" +curso+"]";
	}
	
	public void addAttribute(String string, java.util.List<Asignatura> listaTodasLasAsignaturas) {
		// TODO Auto-generated method stub
		
	}
}
