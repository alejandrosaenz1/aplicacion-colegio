package com.crud.h2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import antlr.collections.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Curso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private String grado;
	@Column(nullable = false)
	private String salon;
	@Column(nullable = false)
	private String asignaturas;
	
	public Curso() {
		
	}

	public Curso(Long id, String grado, String salon, String asignaturas) {
		super();
		this.id = id;
		this.grado = grado;
		this.salon = salon;
		this.asignaturas = asignaturas;
	}
	public Curso(String grado, String salon, String asignaturas) {
		super();
		this.grado = grado;
		this.salon = salon;
		this.asignaturas = asignaturas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public String getSalon() {
		return salon;
	}

	public void setSalon(String salon) {
		this.salon = salon;
	}

	public String getAsignaturas() {
		return asignaturas;
	}

	public void setAsignaturas(String asignaturas) {
		this.asignaturas = asignaturas;
	}
	
	@Override
	public String toString() {
		return "Curso [id=" +id+", grado=" +grado+ ", salon=" +salon+", asignaturas=" +asignaturas+"]";
	}



	public void addAttribute(String string, java.util.List<Curso> listaTodosLosCursos) {
		// TODO Auto-generated method stub
		
	}
	
	
}
