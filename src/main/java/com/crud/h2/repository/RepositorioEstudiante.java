package com.crud.h2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crud.h2.model.Estudiante;

@Repository
public interface RepositorioEstudiante extends JpaRepository<Estudiante, Long>{

	
}
