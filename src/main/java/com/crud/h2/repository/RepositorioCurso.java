package com.crud.h2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.h2.model.Curso;

public interface RepositorioCurso extends JpaRepository<Curso, Long>{

}
