package com.crud.h2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.h2.model.Profesor;

public interface RepositorioProfesor extends JpaRepository<Profesor, Long>{

}
