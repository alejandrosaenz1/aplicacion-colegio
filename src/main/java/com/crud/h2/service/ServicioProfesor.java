package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import com.crud.h2.model.Profesor;

public interface ServicioProfesor {
	
	public List<Profesor> listaTodosLosProfesores();

	public Profesor guardarProfesor(Profesor profesor);

	public Profesor obtenerProfesor(Long idProfesor);

	public Profesor profesorAModificar(Profesor profesor);

	boolean eliminarProfesor(Long id);
}
