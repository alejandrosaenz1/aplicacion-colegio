package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.model.Curso;
import com.crud.h2.repository.RepositorioCurso;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ServicioCursoImpl implements ServicioCurso{

	@Autowired
	private RepositorioCurso repositorioCurso;
	
	@Override
	public List<Curso> listaTodosLosCursos(){
		
		return repositorioCurso.findAll();
	}
	
	@Override
	public Curso guardarCurso(Curso curso) {
		
		return repositorioCurso.save(curso);
	}

	@Override
	public Curso obtenerCurso(Long idCurso) {
		
		return repositorioCurso.findById(idCurso).orElseThrow(() -> {throw new RuntimeException();});
	}

	@Override
	public Curso cursoAModificar(Curso curso) {
		
		return repositorioCurso.save(curso);
	}

	@Override
	public boolean eliminarCurso(Long id) {
		try {
			repositorioCurso.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	
}
