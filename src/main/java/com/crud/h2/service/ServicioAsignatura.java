package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import com.crud.h2.model.Asignatura;

public interface ServicioAsignatura {
	
	public List<Asignatura> listaTodasLasAsignaturas();

	public Asignatura guardarAsignatura(Asignatura asignatura);

	public Asignatura obtenerAsignatura(Long idAsignatura);

	public Asignatura asignaturaAModificar(Asignatura asignatura);

	boolean eliminarAsignatura(Long id);
}
