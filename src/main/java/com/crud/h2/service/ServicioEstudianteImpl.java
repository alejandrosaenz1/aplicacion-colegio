package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.model.Estudiante;
import com.crud.h2.repository.RepositorioEstudiante;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ServicioEstudianteImpl implements ServicioEstudiante{
	
	
	@Autowired
	private RepositorioEstudiante repositorioEstudiante;
	
		
	@Override
	public List<Estudiante> listaTodosLosEstudiantes() {
		
		return repositorioEstudiante.findAll();
	}
	
	@Override
	public Estudiante guardarEstudiante(Estudiante estudiante) {
		
		return repositorioEstudiante.save(estudiante);
	}

	@Override
	public Estudiante obtenerEstudiante(Long idEstudiante) {
		
		return repositorioEstudiante.findById(idEstudiante).orElseThrow(() -> {throw new RuntimeException();});
	}

	
	@Override
	public Estudiante estudianteAModificar(Estudiante estudiante) {
		
		return repositorioEstudiante.save(estudiante);
	}

	@Override
	public boolean eliminarEstudiante(Long id) {
		try {
			repositorioEstudiante.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	
	
}
