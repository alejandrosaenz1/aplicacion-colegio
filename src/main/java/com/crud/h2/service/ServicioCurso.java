package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import com.crud.h2.model.Curso;

public interface ServicioCurso {
	
	public List<Curso> listaTodosLosCursos();

	public Curso guardarCurso(Curso curso);

	public Curso obtenerCurso(Long idCurso);

	public Curso cursoAModificar(Curso curso);

	boolean eliminarCurso(Long id);
}
