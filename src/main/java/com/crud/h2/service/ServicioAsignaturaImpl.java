package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.model.Asignatura;
import com.crud.h2.repository.RepositorioAsignatura;
import com.crud.h2.repository.RepositorioCurso;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ServicioAsignaturaImpl implements ServicioAsignatura{
	
	@Autowired
	private RepositorioAsignatura repositorioAsignatura;

	@Override
	public List<Asignatura> listaTodasLasAsignaturas(){
		
		return repositorioAsignatura.findAll();
	}
	
	@Override
	public Asignatura guardarAsignatura(Asignatura asignatura) {
		
		return repositorioAsignatura.save(asignatura);
	}
	
	@Override
	public Asignatura obtenerAsignatura(Long idAsignatura) {
		
		return repositorioAsignatura.findById(idAsignatura).orElseThrow(() -> {throw new RuntimeException();});
	}

	@Override
	public Asignatura asignaturaAModificar(Asignatura asignatura) {

		return repositorioAsignatura.save(asignatura);
	}

	@Override
	public boolean eliminarAsignatura(Long id) {
		try {
			repositorioAsignatura.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
