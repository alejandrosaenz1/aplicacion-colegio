package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.model.Profesor;
import com.crud.h2.repository.RepositorioProfesor;

@Service
public class ServicioProfesorImpl implements ServicioProfesor{
	
	@Autowired
	public RepositorioProfesor repositorioProfesor;
	
	@Override
	public List<Profesor> listaTodosLosProfesores(){
		return repositorioProfesor.findAll();
	}
	
	@Override
	public Profesor guardarProfesor(Profesor profesor) {
		
		return repositorioProfesor.save(profesor);
	}

	@Override
	public Profesor obtenerProfesor(Long idProfesor) {
		
		return repositorioProfesor.findById(idProfesor).orElseThrow(() -> {throw new RuntimeException();});
	}

	@Override
	public Profesor profesorAModificar(Profesor profesor) {
		
		return repositorioProfesor.save(profesor);
	}

	@Override
	public boolean eliminarProfesor(Long id) {
		try {
			repositorioProfesor.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

		

}
