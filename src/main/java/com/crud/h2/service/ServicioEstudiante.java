package com.crud.h2.service;

import com.crud.h2.model.Estudiante;

import java.util.List;

public interface ServicioEstudiante {
	
	public List<Estudiante> listaTodosLosEstudiantes();
	
	public Estudiante guardarEstudiante(Estudiante estudiante);
	
	public Estudiante obtenerEstudiante(Long idEstudiante);
	
	public Estudiante estudianteAModificar(Estudiante estudiante);
	
	boolean eliminarEstudiante(Long id);
	
}
