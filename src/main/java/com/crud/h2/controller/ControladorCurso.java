package com.crud.h2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.model.Curso;
import com.crud.h2.model.Estudiante;
import com.crud.h2.service.ServicioCurso;

import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/curso")
@RequiredArgsConstructor
public class ControladorCurso {

	@Autowired
	ServicioCurso servicioCurso;
	
	@GetMapping({"/listar"})
	public ResponseEntity listarCursos(Curso curso) {
		curso.addAttribute("curso", servicioCurso.listaTodosLosCursos());
		return new ResponseEntity(servicioCurso.listaTodosLosCursos(), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity guardarCurso(@RequestBody Curso curso) {
		return new ResponseEntity<Curso>(servicioCurso.guardarCurso(curso),HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity obtenerCurso(@PathVariable("id") Long idCurso) {
		return new ResponseEntity<Curso>(servicioCurso.obtenerCurso(idCurso),HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity modificarCurso(@PathVariable("id") Long idCurso,@RequestBody Curso curso) {
		Curso cursoBuscado = servicioCurso.obtenerCurso(idCurso);
		cursoBuscado.setSalon(cursoBuscado.getSalon());
		servicioCurso.cursoAModificar(cursoBuscado);
		return new ResponseEntity(servicioCurso.cursoAModificar(cursoBuscado),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity eliminarCurso(@PathVariable("id") Long idCurso) {
		boolean respuesta = servicioCurso.eliminarCurso(idCurso);
		if(respuesta == true) {
			return new ResponseEntity<Object>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
