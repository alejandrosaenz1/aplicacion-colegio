package com.crud.h2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.model.Asignatura;
import com.crud.h2.model.Estudiante;
import com.crud.h2.repository.RepositorioAsignatura;
import com.crud.h2.service.ServicioAsignatura;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/asignatura")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200/")
public class ControladorAsignatura {

	@Autowired
	private ServicioAsignatura servicioAsignatura;
	
	@Autowired
	private RepositorioAsignatura repositorioAsignatura;

	// Este metodo sirve para listar todas las asignaturas
	@GetMapping({ "/listar" })
	public ResponseEntity listarAsignatura(Asignatura asignatura) {
		asignatura.addAttribute("estudiantes", servicioAsignatura.listaTodasLasAsignaturas());
		return new ResponseEntity(servicioAsignatura.listaTodasLasAsignaturas(), HttpStatus.OK);
	}

	// Este metodo sirve para guardar una asignatura
	@PostMapping
	public ResponseEntity guardarAsignatura(@RequestBody Asignatura asignatura) {
		return new ResponseEntity(servicioAsignatura.guardarAsignatura(asignatura), HttpStatus.CREATED);
	}

	// Este metodo sirve para obtener una asignatura
	@GetMapping("/{id}")
	public ResponseEntity obtenerAsignatura(@PathVariable("id") Long idAsignatura) {
		return new ResponseEntity(servicioAsignatura.obtenerAsignatura(idAsignatura), HttpStatus.OK);
	}

	// este metodo sirve para modificar una asignatura
	@PutMapping("/{id}")
	public ResponseEntity modificarAsignatura(@PathVariable("id") Long idAsignatura,
			@RequestBody Asignatura asignatura) {

		Asignatura asignaturaBuscada = servicioAsignatura.obtenerAsignatura(idAsignatura);
		asignaturaBuscada.setCurso(asignaturaBuscada.getCurso());
		servicioAsignatura.asignaturaAModificar(asignaturaBuscada);
		Asignatura asginaturaActualizada = repositorioAsignatura.save(asignatura);
		return ResponseEntity.ok(asginaturaActualizada);
		
	}

	// Este metodo sirve para eliminar una asignatura
	@DeleteMapping("/{id}")
	public ResponseEntity eliminarAsignatura(@PathVariable("id") Long idAsignatura) {
		boolean respuesta = servicioAsignatura.eliminarAsignatura(idAsignatura);
		if (respuesta == true) {
			return new ResponseEntity(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
