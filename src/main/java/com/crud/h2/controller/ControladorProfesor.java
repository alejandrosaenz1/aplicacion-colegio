package com.crud.h2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.model.Estudiante;
import com.crud.h2.model.Profesor;
import com.crud.h2.service.ServicioEstudiante;
import com.crud.h2.service.ServicioProfesor;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/profesores")
@RequiredArgsConstructor
public class ControladorProfesor {

	@Autowired
	ServicioProfesor servicioProfesor;
	
	
	
	@GetMapping({"/listar"})
	public ResponseEntity listarProfesores(Profesor profesor) {
		profesor.addAttribute("profesore", servicioProfesor.listaTodosLosProfesores());
		return new ResponseEntity(servicioProfesor.listaTodosLosProfesores(), HttpStatus.OK);
	}
	

	@PostMapping
	public ResponseEntity guardarProfesor(@RequestBody Profesor profesor) {
		return new ResponseEntity(servicioProfesor.guardarProfesor(profesor),HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity obtenerProfesor(@PathVariable("id") Long idProfesor) {
		return new ResponseEntity(servicioProfesor.obtenerProfesor(idProfesor),HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity modificarProfesor(@PathVariable("id") Long idProfesor,@RequestBody Profesor profesor) {
		Profesor ProfesorBuscado = servicioProfesor.obtenerProfesor(idProfesor);
		ProfesorBuscado.setAsignaturas(ProfesorBuscado.getAsignaturas());
		ProfesorBuscado.setNombre(ProfesorBuscado.getNombre());
		servicioProfesor.profesorAModificar(ProfesorBuscado);
		return new ResponseEntity(servicioProfesor.guardarProfesor(ProfesorBuscado), HttpStatus.OK);
		//return new ResponseEntity(servicioEstudiante.estudianteAModificar(estudiante),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity eliminarProfesor(@PathVariable("id") Long idProfesor) {
		boolean respuesta = servicioProfesor.eliminarProfesor(idProfesor);
		if(respuesta == true) {
			return new ResponseEntity(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
