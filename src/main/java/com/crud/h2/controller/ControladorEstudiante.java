package com.crud.h2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.model.Estudiante;
import com.crud.h2.service.ServicioEstudiante;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/estudiantes")
@RequiredArgsConstructor
public class ControladorEstudiante {
	
	@Autowired
	ServicioEstudiante servicioEstudiante;
	
	
	
	@GetMapping({"/listar"})
	public ResponseEntity listarEstudiantes(Estudiante estudiante) {
		estudiante.addAttribute("estudiantes", servicioEstudiante.listaTodosLosEstudiantes());
		return new ResponseEntity(servicioEstudiante.listaTodosLosEstudiantes(), HttpStatus.OK);
	}
	

	@PostMapping
	public ResponseEntity guardarEstudiante(@RequestBody Estudiante estudiante) {
		return new ResponseEntity(servicioEstudiante.guardarEstudiante(estudiante),HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity obtenerEstudiante(@PathVariable("id") Long idEstudiante) {
		return new ResponseEntity(servicioEstudiante.obtenerEstudiante(idEstudiante),HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity modificarEstudiante(@PathVariable("id") Long idEstudiante,@RequestBody Estudiante estudiante) {
		Estudiante estudianteBuscado = servicioEstudiante.obtenerEstudiante(idEstudiante);
		estudianteBuscado.setNombre(estudianteBuscado.getNombre());
		estudianteBuscado.setAsignaturas(estudianteBuscado.getAsignaturas());
		servicioEstudiante.estudianteAModificar(estudianteBuscado);
		return new ResponseEntity(servicioEstudiante.guardarEstudiante(estudianteBuscado), HttpStatus.OK);
		//return new ResponseEntity(servicioEstudiante.estudianteAModificar(estudiante),HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity eliminarEstudiante(@PathVariable("id") Long idEstudiante) {
		boolean respuesta = servicioEstudiante.eliminarEstudiante(idEstudiante);
		if(respuesta == true) {
			return new ResponseEntity(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
